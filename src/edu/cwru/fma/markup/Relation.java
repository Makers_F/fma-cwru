package edu.cwru.fma.markup;

public enum Relation {
	SUBCLASS("(s)"),
	PART("(p)");

	public final String marker;

	Relation(String s) {
		marker = s;
	}

	public static Relation find(String relation) {
		for(Relation r : Relation.values()) {
			if (relation.equals(r.marker))
				return r;
		}
		throw new IllegalArgumentException("It doesn't exist any relation with marker " + relation);
	}

	/**
	 * @return SUBCLASS only if both are SUBCLASS
	 */
	public static Relation min(Relation r1, Relation r2) {
		// if both are SUBCLASS return SUBCLASS, PART otherwise
		return r1 == SUBCLASS ? r2 : PART;
	}

	/**
	 * @return SUBCLASS if even only one is SUBCLASS
	 */
	public static Relation max(Relation r1, Relation r2) {
		// if both are SUBCLASS return SUBCLASS, PART otherwise
		return r1 == SUBCLASS ? SUBCLASS : r2;
	}
}