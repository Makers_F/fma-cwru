package edu.cwru.fma.markup;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TransitiveClosureMarkup {

	private static final String PARENT_MARKER = "p";
	private static final String CHILD_MARKER = "c";

	public static String setDistance(String element, int distance) {
		return distance + "|" + removeDistance(element);
	}

	public static int getDistance(String element) {
		return Integer.parseInt(element.substring(0, element.indexOf('|')));
	}

	public static String removeDistance(String element) {
		//in case the string is not found, then index = 0, so is like not calling substring
		int index = element.indexOf('|') + 1;
		return element.substring(index);
	}

	public static String makeParent(String element) {
		return PARENT_MARKER + element;
	}

	public static boolean isParent(String element) {
		return element.startsWith(PARENT_MARKER);
	}

	public static String unmakeParent(String parent) {
		return parent.substring(PARENT_MARKER.length());
	}

	public static String makeChild(String element) {
		return CHILD_MARKER + element;
	}

	public static boolean isChild(String element) {
		return element.startsWith(CHILD_MARKER);
	}

	public static String unmakeChild(String child) {
		return child.substring(CHILD_MARKER.length());
	}

	public static String makeAssessed(List<String> elements) {
		StringBuilder sb = new StringBuilder();
		String prefix = "[";
		for (String element : elements) {
		  sb.append(prefix);
		  prefix = ",";
		  sb.append(element);
		}
		sb.append("]");
		return sb.toString();
	}

	public static <T> String makeAssessed(List<T> elements, Converter<T, String> converter) {
		StringBuilder sb = new StringBuilder();
		String prefix = "[";
		for (T element : elements) {
		  sb.append(prefix);
		  prefix = ",";
		  sb.append(converter.convert(element));
		}
		sb.append("]");
		return sb.toString();
	}
	public static boolean isAssessed(String element) {
		return element.startsWith("[") && element.endsWith("]");
	}

	public static List<String> unmakeAssessed(String assessed) {
		List<String> splitted = new LinkedList<String>();
		for (String element : assessed.substring(1, assessed.length() - 1).split(",")) {
			splitted.add(element);
		}
		return splitted;
	}

	public static <T> Collection<T>  unmakeAssessed(Collection<T> collection, String assessed, Converter<String, T> converter) {
		for (String element : assessed.substring(1, assessed.length() - 1).split(",")) {
			collection.add(converter.convert(element));
		}
		return collection;
	}

	public static Collection<String>  unmakeAssessed(Collection<String> collection, String assessed, Filter<String> filter) {
		for (String element : assessed.substring(1, assessed.length() - 1).split(",")) {
			collection.add(filter.filter(element));
		}
		return collection;
	}

	public static boolean isRelationMarked(String element) {
		return element.endsWith(Relation.SUBCLASS.marker) ||
				element.endsWith(Relation.PART.marker);
	}

	public static Relation getRelation(String element) {
		return Relation.find(element.substring(element.length()-3, element.length()));
	}

	public static String setRelation(String element, Relation relation) {
		if(isRelationMarked(element)) {
			return removeRelation(element) + relation.marker;
		} else {
			return element + relation.marker;
		}
	}

	public static String removeRelation(String element) {
		return element.substring(0, element.length()-3);
	}

	public static interface Filter<T> {
		T filter(T element);
	}

	public static interface Converter<O,C> {
		C convert(O original);
	}
}
