package edu.cwru.fma.parsing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import edu.cwru.fma.markup.TransitiveClosureMarkup;
import edu.cwru.fma.markup.TransitiveClosureMarkup.Converter;
import edu.cwru.fma.transitiveclosure.customdata.ConnectedElements;
import edu.cwru.fma.transitiveclosure.customdata.FoundEdge;
import edu.cwru.fma.transitiveclosure.customdata.ValueTypeSwitch;

public class TextHasher {

	private int ID;
	private BiMap<String, Integer> hashMap = HashBiMap.create();
	BiMap<Integer, String> inverseMap;

	private Converter<String, FoundEdge> converterFromString = new TransitiveClosureMarkup.Converter<String, FoundEdge>() {
		@Override
		public FoundEdge convert(String original) {
			FoundEdge converted = new FoundEdge();
			edgeFromString(converted, original);
			return converted;	
		}};
		
	private Converter<FoundEdge, String> converterToString = new Converter<FoundEdge, String>() {
		@Override
		public String convert(FoundEdge original) {
			return stringFromEdge(original);
		}};

	public void convertToSequenceFile(String inputPath, String outputPath, String mapPath) throws IOException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(new Path(inputPath))));
		SequenceFile.Writer writer = SequenceFile.createWriter(conf, SequenceFile.Writer.file(new Path(outputPath)),
																		SequenceFile.Writer.keyClass(IntWritable.class),
																		SequenceFile.Writer.valueClass(ValueTypeSwitch.class),
																		SequenceFile.Writer.compression(CompressionType.NONE));

		ValueTypeSwitch valueWritable = new ValueTypeSwitch();
		FoundEdge edge = new FoundEdge();

		for(String line = reader.readLine(); line != null; line = reader.readLine()) {
			String[] split= line.split("\t");
			String key = split[0];
			String value = split[1];
			Integer keyHash = hashMap.get(key);
			if(keyHash == null) {
				keyHash = new Integer(ID); 
				hashMap.put(key, keyHash.intValue());
				ID++;
			}
			if(TransitiveClosureMarkup.isAssessed(value)) {
				List<FoundEdge> collection = new LinkedList<FoundEdge>();
				TransitiveClosureMarkup.unmakeAssessed(collection, value, converterFromString);
				ConnectedElements connected = new ConnectedElements();
				connected.setConnectedElements(collection);
				valueWritable.setConnectedElements(connected);
				writer.append(new IntWritable(keyHash), valueWritable);
			} else {
				edgeFromString(edge, value);
				valueWritable.setFoundEdge(edge, false);
				writer.append(new IntWritable(keyHash), valueWritable);
			}
		}
		writer.close();
		reader.close();
		ObjectOutputStream oos = new ObjectOutputStream(fs.create(new Path(mapPath), true));
		oos.writeObject(hashMap);
		oos.close();
	}

	private void edgeFromString(FoundEdge edge, String string) {
		String cleanValue = TransitiveClosureMarkup.removeDistance(TransitiveClosureMarkup.removeRelation(string));
		Integer valueHash = hashMap.get(cleanValue);
		if(valueHash == null) {
			valueHash = new Integer(ID);
			hashMap.put(cleanValue, valueHash);
			ID++;
		}
		edge.setID(valueHash.intValue());
		edge.setDistance(TransitiveClosureMarkup.getDistance(string));
		edge.setRelationType(TransitiveClosureMarkup.getRelation(string));
	}

	@SuppressWarnings("unchecked")
	public void convertFromSequenceFile(String inputPath, String outputPath, String mapPath) throws IOException, ClassNotFoundException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		ObjectInputStream ois = new ObjectInputStream(fs.open(new Path(mapPath)));
		Object readObject = ois.readObject();
		if (BiMap.class.isAssignableFrom(readObject.getClass())) {
			hashMap = (BiMap<String, Integer>) readObject;
		} else {
			throw new RuntimeException("The read object is not a bimap");
		}
		ois.close();
		inverseMap = hashMap.inverse();
		SequenceFile.Reader reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(new Path(inputPath)));
		Writer writer = new BufferedWriter(new OutputStreamWriter(fs.create(new Path(outputPath), true)));
		IntWritable key = new IntWritable();
		ValueTypeSwitch value = new ValueTypeSwitch();
		while(reader.next(key, value)) {
			writer.write(inverseMap.get(new Integer(key.get())));
			writer.write("\t");
			if(value.isConnectedElements()) {
				List<FoundEdge> collection = value.getConnectedElements().getConnectedElements();
				writer.write(TransitiveClosureMarkup.makeAssessed(collection, converterToString));
				writer.write("\n");
			} else {
				writer.write(stringFromEdge(value.getFoundEdge()));
				writer.write("\n");
			}
		}
		reader.close();
		writer.close();
	}

	private String stringFromEdge(FoundEdge edge) {
		String name = inverseMap.get(new Integer(edge.getID()));
		return TransitiveClosureMarkup.setRelation(TransitiveClosureMarkup.setDistance(name, edge.getDistance()), edge.getRelationType());
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		if(args[0].equals("hash")) {
			new TextHasher().convertToSequenceFile(args[1], args[2], args[3]);
		} else if(args[0].equals("dehash")) {
			new TextHasher().convertFromSequenceFile(args[1], args[2], args[3]);
		}
	}
}
