package edu.cwru.fma.parsing;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import edu.cwru.fma.markup.Relation;
import edu.cwru.fma.markup.TransitiveClosureMarkup;

public class FMAParser {

	private static final String NAMESPACE = "http://purl.org/sig/fma#";

	private static final int BUFFER_SIZE = 10 * 1024 * 1024; // 10 MB
	private static final int RETURN_OK = 1;
	private static final int RETURN_EOF = 2;
	private static final int RETURN_SKIPPED = 3;
	private static final int RETURN_SKIPPED_IGNORED = 4;
	private static final int RETURN_SKIPPED_EMPTY_NAME = 5;

	//not in the pattern: related
	private static final Pattern PART_OF_NAME = Pattern.compile("(constitutional|regional)_part(_of)?");

	public static void parse(File inputFile, File outputFile, PrintStream log) throws XMLStreamException, FactoryConfigurationError, IOException {
		InputStream input = new BufferedInputStream(new FileInputStream(inputFile), BUFFER_SIZE);
		XMLEventReader reader = XMLInputFactory.newInstance().createXMLEventReader(input);
		
		Writer output =  new BufferedWriter(new FileWriter(outputFile), BUFFER_SIZE);
		
		//skip the RDF declaration
		while(reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if(event.isStartElement()) {
				if(event.asStartElement().getName().getLocalPart().equals("RDF"))
					break;
				else {
					output.close();
					throw new RuntimeException("The document is not a valid RDF one");
				}
			}
		}
		boolean success = true;
		int totalIgnored = 0;
		int totalEmptyName = 0;
		while(reader.hasNext() && success) {
			try {
				int returncode = parseElement(reader, output, log);
				if (returncode == RETURN_SKIPPED_IGNORED)
					totalIgnored++;
				if (returncode == RETURN_SKIPPED_EMPTY_NAME)
					totalEmptyName++;
				success = (returncode != RETURN_EOF);
			} catch (XMLStreamException e) {
				throw new RuntimeException(e);
			}
		}
		log.println("Ignored " + totalIgnored + " elements.");
		log.println("Skipped " + totalEmptyName + " due to empty name.");
		output.close();
	}

	private static int parseElement(XMLEventReader reader, Writer output, PrintStream log) throws XMLStreamException, IOException {
		boolean ignore = false;
		int currentLevel = 0;
		String name = "";
		List<String> subClassOf = new LinkedList<String>();
		List<String> partOf = new LinkedList<String>();
		external:
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if(event.isStartElement()) {
				currentLevel++;
				StartElement startElement = event.asStartElement();

				if(startElement.getName().getLocalPart().equals("Description")) {
					@SuppressWarnings("unchecked")
					Iterator<Attribute> attributes = startElement.getAttributes();
					while(attributes.hasNext()) {
						Attribute attribute = attributes.next();
						if(attribute.getName().getLocalPart().equals("about")) {
							String value = attribute.getValue(); 
							if(value.contains(NAMESPACE)) {
								name = fixFormat(value);
							}
							else {
								continue external;
							}
						} else {
							if(attribute.getName().getLocalPart().equals("nodeID")) {
								ignore = true;
								continue external;
							}
							log.println("Unexpected attribute " + attribute.getName().getLocalPart());
						}
					}
				} else if(startElement.getName().getLocalPart().equals("label")) {
					String label = reader.nextEvent().asCharacters().getData();
					if(name.equals("")) {
						name = fixFormat(label);
					} else if (!name.equals(fixFormat(label))) {
						log.println("Name and label are different. Name: " + name + ", Label: " + label);
					}
				} else if(startElement.getName().getLocalPart().equals("subClassOf")) {
					@SuppressWarnings("unchecked")
					Iterator<Attribute> attributes = startElement.getAttributes();
					while(attributes.hasNext()) {
						Attribute attribute = attributes.next();
						if(attribute.getName().getLocalPart().equals("resource")) {
							subClassOf.add(fixFormat(attribute.getValue()));
						}
					}
				} else if(startElement.getName().getLocalPart().equals("type")) {
					@SuppressWarnings("unchecked")
					Iterator<Attribute> attributes = startElement.getAttributes();
					while(attributes.hasNext()) {
						Attribute attribute = attributes.next();
						if(attribute.getName().getLocalPart().equals("resource")) {
							if(attribute.getValue().contains("Property") ||
									attribute.getValue().contains("Concept_name"))
								ignore = true;
						}
					}
				} else if(PART_OF_NAME.matcher(startElement.getName().getLocalPart()).matches()) {
					@SuppressWarnings("unchecked")
					Iterator<Attribute> attributes = startElement.getAttributes();
					while(attributes.hasNext()) {
						Attribute attribute = attributes.next();
						if(attribute.getName().getLocalPart().equals("resource")) {
							partOf.add(fixFormat(attribute.getValue()));
						}
					}
				}
			}

			if(event.isEndElement()) {
				currentLevel--;
				EndElement endElement = event.asEndElement();

				if(currentLevel == 0) {
					if(!endElement.getName().getLocalPart().equals("Description")) {
						log.println("End Element " + endElement.getName() + " at level 0.");
						return RETURN_SKIPPED;
					} else if(name.equals("")) {
						if (!subClassOf.isEmpty())
							log.println("Name empty but subClass not. First subClass: " + subClassOf.get(0));
						if (!partOf.isEmpty())
							log.println("Name empty but partOf not. First subClass: " + partOf.get(0));
						return RETURN_SKIPPED_EMPTY_NAME;
					} else if (ignore) {
						return RETURN_SKIPPED_IGNORED;
					} else {
						for (String sub : subClassOf) {
							output.write((name + "\t" + "1|" + TransitiveClosureMarkup.setRelation(sub, Relation.SUBCLASS) + "\n").toLowerCase());
						}
						for (String part : partOf) {
							output.write((name + "\t" + "1|" + TransitiveClosureMarkup.setRelation(part, Relation.PART) + "\n").toLowerCase());
						}
						return RETURN_OK;
					}
				} else if (currentLevel == -1) {
					return RETURN_EOF;
				}
			}
		}
		return RETURN_EOF;
	}

	private static String fixFormat(String original) {
		return original.replace(NAMESPACE, "").replaceAll("[\\Q()[]{}',_/:\\E]"," ");
	}

	public static void main(String[] args) throws XMLStreamException, FactoryConfigurationError, IOException {
		File in = new File("C:\\Users\\Francesco\\Desktop\\Utilities\\FMA\\fma3.2.owl");
		File outName = new File("C:\\Users\\Francesco\\Desktop\\Utilities\\FMA\\out.txt");
		PrintStream logStream = new PrintStream(new File("C:\\Users\\Francesco\\Desktop\\Utilities\\FMA\\out_log.txt"));
		logStream.println("Starting parsing.");
		long start = System.currentTimeMillis();
		parse(in, outName, logStream);
		logStream.println("Parsing finished. Total time required: " + (System.currentTimeMillis() - start));
		System.out.println("Total time required to parse: " + (System.currentTimeMillis() - start));
	}
}
