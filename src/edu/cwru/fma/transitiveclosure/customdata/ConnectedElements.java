package edu.cwru.fma.transitiveclosure.customdata;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.io.WritableComparable;

public class ConnectedElements implements WritableComparable<ConnectedElements> {

	private List<FoundEdge> connectedElements;

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(connectedElements.size());
		for(FoundEdge fe : connectedElements ) {
			fe.write(out);
		}
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		int size = in.readInt();
		connectedElements = new LinkedList<FoundEdge>();
		for(int i = 0; i < size; i++) {
			FoundEdge fe = new FoundEdge();
			fe.readFields(in);
			connectedElements.add(fe);
		}
	}

	@Override
	public int compareTo(ConnectedElements o) {
		if (connectedElements.size() != o.connectedElements.size()) {
			return connectedElements.size() < o.connectedElements.size() ? -1 : 1; 
		} else {
			for(int i = 0; i < connectedElements.size(); i++) {
				int comparison = connectedElements.get(i).compareTo(o.connectedElements.get(i));
				if (comparison != 0) {
					return comparison;
				}
			}
			return 0;
		}
	}

	public List<FoundEdge> getConnectedElements() {
		return connectedElements;
	}

	public void setConnectedElements(List<FoundEdge> pConnectedElements) {
		connectedElements = pConnectedElements;
	}
}
