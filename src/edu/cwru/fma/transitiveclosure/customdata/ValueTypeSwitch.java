package edu.cwru.fma.transitiveclosure.customdata;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class ValueTypeSwitch implements WritableComparable<ValueTypeSwitch> {

	private boolean isConnectedElements;
	private ConnectedElements connectedElements;
	private FoundEdge foundEdge;
	//only valid when isConnectedElements == false
	private boolean isChild;

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeBoolean(isConnectedElements);
		if(isConnectedElements) {
			connectedElements.write(out);
		} else {
			out.writeBoolean(isChild);
			foundEdge.write(out);
		}
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		isConnectedElements = in.readBoolean();
		if(isConnectedElements) {
			connectedElements = new ConnectedElements();
			connectedElements.readFields(in);
		} else {
			isChild = in.readBoolean();
			foundEdge = new FoundEdge();
			foundEdge.readFields(in);
		}
		
	}

	/**
	 * When it stores connectedElements it is less than when it stores foundEdges. 
	 * If it stores the same things, then compare the two classes.
	 */
	@Override
	public int compareTo(ValueTypeSwitch o) {
		if (isConnectedElements != o.isConnectedElements()) {
			return isConnectedElements == true ? -1 : 1;
		} else {
			if(isConnectedElements) {
				return connectedElements.compareTo(o.getConnectedElements());
			} else {
				return foundEdge.compareTo(o.getFoundEdge());
			}
		}
	}

	public boolean isConnectedElements() {
		return isConnectedElements;
	}

	public ConnectedElements getConnectedElements() {
		if (isConnectedElements) {
			return connectedElements;
		} else {
			throw new IllegalStateException("The ValueTypeSwitch does not contain a connected element.");
		}
	}

	public void setConnectedElements(ConnectedElements pConnectedElements) {
		connectedElements = pConnectedElements;
		isConnectedElements = true;
		foundEdge = null;
	}

	public FoundEdge getFoundEdge() {
		if(!isConnectedElements) {
			return foundEdge;
		} else {
			throw new IllegalStateException("The ValueTypeSwitch does not contain a found edge.");
		}
	}

	public void setFoundEdge(FoundEdge pFoundEdge, boolean pIsChild) {
		foundEdge = pFoundEdge;
		isConnectedElements = false;
		connectedElements = null;
		isChild = pIsChild;
	}

	public boolean isChild() {
		if(!isConnectedElements)
			return isChild;
		else
			throw new IllegalStateException("The field isChild is only valid when isConnectedElements is false.");
	}

	public void setChild(boolean pIsChild) {
		if(!isConnectedElements)
			isChild = pIsChild;
		else
			throw new IllegalStateException("The field isChild is only valid when isConnectedElements is false.");
	}
}
