package edu.cwru.fma.transitiveclosure.customdata;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

import edu.cwru.fma.markup.Relation;

public class FoundEdge implements WritableComparable<FoundEdge> {

	public FoundEdge() {}

	public FoundEdge(int pID, int pDistance, Relation pRelation) {
		id = pID;
		distance = pDistance;
		relationType = pRelation;
	}

	public FoundEdge(FoundEdge copy) {
		this(copy.getID(), copy.getDistance(), copy.getRelationType());
	}

	private int id;
	private int distance;
	private Relation relationType;

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(id);
		out.writeInt(distance);
		out.writeInt(getIndex(relationType));

	}

	@Override
	public void readFields(DataInput in) throws IOException {
		id = in.readInt();
		distance = in.readInt();
		relationType = getEnumFromIndex(in.readInt(), Relation.class);

	}

	@Override
	public int compareTo(FoundEdge o) {
		if(relationType != o.relationType) {
			return relationType == Relation.SUBCLASS ? -1 : 1;
		} else {
			if (distance == o.distance) {
				return id == o.id ? 0 : (id < o.id ? -1 : 1);
			} else {
				return distance < o.distance ? -1 : 1;
			}
		}
	}

	public int getID() {
		return id;
	}

	public void setID(int pID) {
		this.id = pID;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Relation getRelationType() {
		return relationType;
	}

	public void setRelationType(Relation relationType) {
		this.relationType = relationType;
	}

	public static <T extends Enum<T>> int getIndex(T value) {
		int i = 0;
		for(Enum<?> val : value.getClass().getEnumConstants()) {
			if(val == value) {
				return i;
			} else {
				i++;
			}
		}
		throw new IllegalArgumentException("Impossible to find the index of the enum.");
	}

	public static <T extends Enum<T>> T getEnumFromIndex(int index, Class<T> enumType) {
		return enumType.getEnumConstants()[index];
	}

}
