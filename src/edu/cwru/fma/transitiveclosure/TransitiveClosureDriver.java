package edu.cwru.fma.transitiveclosure;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import edu.cwru.fma.transitiveclosure.customdata.ValueTypeSwitch;

public class TransitiveClosureDriver extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		Configuration conf = getConf();
		FileSystem fs = FileSystem.get(conf);

		Path originalInput = new Path(args[0]);
		Path originalOutput = new Path(args[1]);

		Path temp1 = new Path(args[2]);
		Path temp2 = new Path(args[3]);
		Path stagingOutput = new Path(args[4]);
		fs.delete(stagingOutput, true);

		Path input = originalInput;
		Path output = temp2;
		fs.delete(temp2, true);

		int steps = 0;

		long total_generated_children = 0;
		int max_numer_of_steps = Integer.MAX_VALUE;
		if(args.length >= 6)
			max_numer_of_steps = Integer.parseInt(args[5]);

		boolean needs_computation = steps < max_numer_of_steps;
		while (needs_computation) {
			Job transitive_closure = getTransitiveClosureJob(conf, steps);
	
			FileInputFormat.addInputPath(transitive_closure, input);
			FileOutputFormat.setOutputPath(transitive_closure, stagingOutput);
	
			System.out.println("Current input is: " + input.toUri().getPath() + ". The current output is: " + output.toUri().getPath());
			transitive_closure.waitForCompletion(true);
			
			long count = transitive_closure.getCounters().findCounter(TransitiveClosureCounter.NUMBER_OF_ADDED_CHILDREN).getValue();
			long generatedChildren = transitive_closure.getCounters().findCounter(TransitiveClosureCounter.NUMBER_OF_NEW_CHILDREN_TO_ANALYZE).getValue();
			total_generated_children += generatedChildren;

			Job removeDuplicates = getRemoveDupliJob(conf);

			FileInputFormat.addInputPath(removeDuplicates, stagingOutput);
			FileOutputFormat.setOutputPath(removeDuplicates, output);

			removeDuplicates.waitForCompletion(true);

			steps++;
			needs_computation = (count != 0) && steps < max_numer_of_steps;
			
			//we need to use the output of the previous step as the input of the next one, so we switch the input and output path.
			//NOTE: both paths must be directories (as the output can not be a file). Moreover, we need to delete the file _SUCCESS generated at
			//the end of every step
			if(needs_computation) {
				// The first iterations uses originalInput as input, but we don't want to use it as outpt directory as well.
				// So we check it here to use the temp directory after the first iteration.
				Path temp = (input == originalInput) ? temp1 : input;
				input = output;
				output = temp;
				//output = new Path(args[3] + steps);
				
				//might not be needed
				fs.delete(Path.mergePaths(input, new Path("/_SUCCESS")), false);
				fs.delete(output, true);
				fs.delete(stagingOutput, true);
			}
		}
		if(!output.equals(originalOutput)) {
			fs.delete(originalOutput, true);
			if(!output.equals(temp2)) {
				fs.delete(temp2, true);
				fs.rename(output, temp2);
			}
			fs.rename(temp2, originalOutput);
			//fs.rename(output, originalOutput);
		}
		fs.close();
		System.out.println("Ended the Transitive Closure in " + steps + ". Total generated children: " + total_generated_children);
		return 1;
	}

	private Job getTransitiveClosureJob(Configuration conf, int steps) throws IOException {
		Job transitive_closure = Job.getInstance(conf, "transitive_closure_" + steps);
		transitive_closure.setJarByClass(TransitiveClosureMapper.class);

		transitive_closure.setMapOutputKeyClass(IntWritable.class);
		transitive_closure.setMapOutputValueClass(ValueTypeSwitch.class);

		transitive_closure.setOutputKeyClass(IntWritable.class);
		transitive_closure.setOutputValueClass(ValueTypeSwitch.class);

		transitive_closure.setMapperClass(TransitiveClosureMapper.class);
		transitive_closure.setReducerClass(TransitiveClosureReducer.class);

		transitive_closure.setInputFormatClass(SequenceFileInputFormat.class);
		transitive_closure.setOutputFormatClass(SequenceFileOutputFormat.class);

		TransitiveClosureReducer.setCurrentStep(transitive_closure, steps);

		return transitive_closure;
	}

	private Job getRemoveDupliJob(Configuration conf) throws IOException {
		Job removeDupli = Job.getInstance(conf, "remove_duplicates");
		removeDupli.setJarByClass(RemoveDuplicatesReducer.class);

		removeDupli.setMapOutputKeyClass(IntWritable.class);
		removeDupli.setMapOutputValueClass(ValueTypeSwitch.class);

		removeDupli.setOutputKeyClass(IntWritable.class);
		removeDupli.setOutputValueClass(ValueTypeSwitch.class);

		removeDupli.setMapperClass(Mapper.class);
		removeDupli.setReducerClass(RemoveDuplicatesReducer.class);

		removeDupli.setInputFormatClass(SequenceFileInputFormat.class);
		removeDupli.setOutputFormatClass(SequenceFileOutputFormat.class);

		return removeDupli;
	}

	public static void main(String[] args) throws Exception {
		ToolRunner.run(new Configuration(), new TransitiveClosureDriver(), args);
	}
}
