package edu.cwru.fma.transitiveclosure;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;

import edu.cwru.fma.markup.Relation;
import edu.cwru.fma.transitiveclosure.customdata.ConnectedElements;
import edu.cwru.fma.transitiveclosure.customdata.FoundEdge;
import edu.cwru.fma.transitiveclosure.customdata.ValueTypeSwitch;

public class TransitiveClosureReducer extends Reducer<IntWritable, ValueTypeSwitch, IntWritable, ValueTypeSwitch>{

	private static final String CURRENT_STEP_URI = "edu.cwru.fma.transitiveclosure.TransitiveClosureReducer.current_step";
	private final IntWritable parentWritable = new IntWritable();
	private final ValueTypeSwitch childSwitch = new ValueTypeSwitch();
	private final ValueTypeSwitch assessedSwitch = new ValueTypeSwitch();

	public static void setCurrentStep(Job job, int step) {
		Configuration conf = job.getConfiguration();
		conf.setInt(CURRENT_STEP_URI, step);
	}

	@Override
	protected void reduce(IntWritable key, Iterable<ValueTypeSwitch> values, Context context) throws IOException, InterruptedException {
		//final int current_step = context.getConfiguration().getInt(CURRENT_STEP_URI, 0);
		List<FoundEdge> children = new LinkedList<FoundEdge>();
		List<FoundEdge> parents = new LinkedList<FoundEdge>();
		ConnectedElements assessed = null;
		for(ValueTypeSwitch value : values) {
			if (value.isConnectedElements()) {
				assessed = value.getConnectedElements();
			} else if(value.isChild()) {
				children.add(value.getFoundEdge());
			} else {
				parents.add(value.getFoundEdge());
			}
		}
		if (assessed == null) {
			assessed = new ConnectedElements();
			assessed.setConnectedElements(new LinkedList<FoundEdge>());
		}
		for(FoundEdge schild : children) {
			updateAssessed(assessed, schild, context);
		}
		if (!assessed.getConnectedElements().isEmpty()) {
			assessedSwitch.setConnectedElements(assessed);
			context.write(key, assessedSwitch);
		}
		//at this step, the childs are the element directly linked to the current leafes (the oldest) of the "ancestorship tree". Since
		// this already contains even the younger ancestors, this are descendant, not only childs.
		FoundEdge child = new FoundEdge();
		for(FoundEdge parent : parents) {
			for(FoundEdge descendant : assessed.getConnectedElements()) {
				context.progress();
				int distance = descendant.getDistance() + parent.getDistance();
				Relation rel = Relation.min(descendant.getRelationType(), parent.getRelationType());
				child.setID(descendant.getID());
				child.setDistance(distance);
				child.setRelationType(rel);
				childSwitch.setFoundEdge(child, true);
				parentWritable.set(parent.getID());
				//the parent must not be related to the current key. All of this is done since the parent has already the key as pair,
				//and here we are bypassing the key, to link the parent to the child of the key.
				if(descendant.getID() != parent.getID()) {
					context.write(parentWritable, childSwitch);
					context.getCounter(TransitiveClosureCounter.NUMBER_OF_NEW_CHILDREN_TO_ANALYZE).increment(1);
				} else {
					context.getCounter(TransitiveClosureCounter.NUMBER_OF_SELF_REFERENCES).increment(1);
				}
			}
		}
	}

	private void updateAssessed(ConnectedElements assessed, FoundEdge element, Context context) {
		ListIterator<FoundEdge> it = assessed.getConnectedElements().listIterator();
		FoundEdge current = new FoundEdge(element);
		boolean newChildren = true;
		//this checks for duplicates and in case it finds any it keeps the most important (the one with the "higher" relation, then shorter distance
		while(it.hasNext()) {
			FoundEdge assessedEdge = it.next();
			if(assessedEdge.getID() == current.getID()) {
				context.getCounter(TransitiveClosureCounter.NUMBER_OF_DUPLICATED_CHILDREN).increment(1);
				it.remove();
				newChildren = false;
				if(assessedEdge.getRelationType() == current.getRelationType()) {
					int distance = Math.min(assessedEdge.getDistance(), current.getDistance());
					current.setDistance(distance);
				} else {
					if(assessedEdge.getRelationType() == Relation.max(assessedEdge.getRelationType(), current.getRelationType())) {
						current.setRelationType(assessedEdge.getRelationType());
						current.setDistance(assessedEdge.getDistance());
					} //else current = current
				}
			}
			context.progress();
		}
		it.add(current);
		if(newChildren) {
			context.getCounter(TransitiveClosureCounter.NUMBER_OF_ADDED_CHILDREN).increment(1);
		}
	}
}
