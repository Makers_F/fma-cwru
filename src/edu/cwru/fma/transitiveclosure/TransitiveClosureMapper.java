package edu.cwru.fma.transitiveclosure;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;

import edu.cwru.fma.transitiveclosure.customdata.ValueTypeSwitch;

/*
 * The terminology in the code is inverted relatively to the actual structure of the graph. 
 * As we are building the transitive closure of each element, we see the actual parent of the element (the parent in the tree above it.
 * So the element is linked to the parent with a is-a or a part-of relationship) is called child. 
 * So, we are actually calling the child the parent, and the parent the child. This because we see the tree as flipped, as we don't care of the "real" child of an element when we
 * build the TC, and the starting element is lone, so it actually looks like a root of a tree.
 */
public class TransitiveClosureMapper extends Mapper<IntWritable, ValueTypeSwitch, IntWritable, ValueTypeSwitch>{

	private final IntWritable childWritable = new IntWritable();

	@Override
	protected void map(IntWritable key, ValueTypeSwitch value, Context context) throws IOException, InterruptedException {
		
		if(value.isConnectedElements()) {
			context.write(key, value);
		} else {
			if(key.get() == value.getFoundEdge().getID()) {
				context.getCounter(TransitiveClosureCounter.NUMBER_OF_SELF_REFERENCES).increment(1);
			} else {
				//output the same pair making sure it is set as child
				value.setChild(true);
				context.write(key, value);

				//output the inverted pair, making sure it is set as parent
				childWritable.set(value.getFoundEdge().getID());
				value.getFoundEdge().setID(key.get());
				value.setChild(false);
				context.write(childWritable, value);
			}
		}
	}
}