package edu.cwru.fma.transitiveclosure;

public enum TransitiveClosureCounter {
	NUMBER_OF_NEW_CHILDREN_TO_ANALYZE,
	NUMBER_OF_DUPLICATED_CHILDREN,
	NUMBER_OF_ADDED_CHILDREN,
	NUMBER_OF_SELF_REFERENCES;
}
