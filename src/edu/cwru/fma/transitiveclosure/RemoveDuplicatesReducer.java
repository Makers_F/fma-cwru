package edu.cwru.fma.transitiveclosure;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import edu.cwru.fma.transitiveclosure.customdata.ConnectedElements;
import edu.cwru.fma.transitiveclosure.customdata.FoundEdge;
import edu.cwru.fma.transitiveclosure.customdata.ValueTypeSwitch;

public class RemoveDuplicatesReducer extends Reducer<IntWritable, ValueTypeSwitch, IntWritable, ValueTypeSwitch> {

	private final ValueTypeSwitch valueWritable = new ValueTypeSwitch();

	@Override
	protected void reduce(IntWritable key, Iterable<ValueTypeSwitch> values, Context context) throws IOException, InterruptedException {
		ConnectedElements assessed = null;
		Map<Integer, FoundEdge> generated = new HashMap<Integer, FoundEdge>();
		for(ValueTypeSwitch value : values) {
			if (value.isConnectedElements()) {
				assessed = value.getConnectedElements();
				context.write(key, value);
			} else {
				FoundEdge stored = generated.get(value.getFoundEdge().getID());
				if (stored == null)
					generated.put(value.getFoundEdge().getID(), value.getFoundEdge());
			}
		}
		//output the element only if it is not present in the assessed elements
		for(FoundEdge edge : assessed.getConnectedElements()) {
			generated.remove(edge.getID());
		}

		for(Entry<Integer, FoundEdge> gen : generated.entrySet()) {
			valueWritable.setFoundEdge(gen.getValue(), true);
			context.write(key, valueWritable);
		}
	}

}
